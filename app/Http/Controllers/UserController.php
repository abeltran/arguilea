<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use PHPUnit\Framework\Exception;
use App\Notifications\changeMail;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('app.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pr = new User();
        return view('app.users.create', compact('pr'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $u = new User();

        $u->name = $rq->nom;
        $u->email = $rq->email;
        $u->password = $rq->password;
        \Log::info($rq->password);
        $u->nickname = $rq->nickname;
        $u->genre = $rq->genre;
        $u->cverole = $rq->cverole;
        $u->created_at = Carbon::now();
        
        if ($u->save()) {
            
            $unew = User::find($u->id);
            
            // $usern = [
            //     'greeting' => 'Hola '.$rq->nom,',',
            //     'body' => 'Bienvenido a la aplicación, te registraste con este correo: '.$rq->email,
            //     'thanks' => 'Gracias por usar mi aplicación',
            // ];

            // \Notification::send($unew, new changeMail($usern));

            toastr('Agregado correctamente');
            return redirect('/home');
        } else {
            toastr('El envio fallo, no se pudo registrar','error');
            return view('app.users.create');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pr = User::find($id);
        return view('app.users.edit', compact('pr'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        try {
            $pr = User::find($id);

            $pr->name = $rq->nom;
            $pr->email = $rq->email;
            $pr->nickname = $rq->nickname;
            $pr->genre = $rq->genre;
            $pr->cverole = $rq->cverole;
            $pr->updated_at = Carbon::now();

            if($pr->save()) {
                toastr('Editado correctamente');

                // $userd = [
                //     'greeting' => 'Hola '.$rq->nom,',',
                //     'body' => 'Su correo ha cambiado a :'.$rq->email,
                //     'thanks' => 'Gracias por usar mi aplicación',
                // ];

                // \Notification::send($pr, new changeMail($userd));

                return redirect('/home');
            }else{
                return view('app.users.edit', compact('pr'));
            }
        }catch(\Illuminate\Database\QueryException $ex){
            toastr('Error:'.$ex->getMessage(), 'warning');
            return back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Auth::user()->id == $id){
            toastr('No puedes eliminarte','error');
            return redirect('/home');
        }else{
            User::destroy($id);
            toastr('Eliminado correctamente');
            return redirect('/home');
        }
        
    }

    public function changeEmail($id){
        $u = User::find($id);
    }
}
