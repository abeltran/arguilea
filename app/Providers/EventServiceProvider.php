<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Carbon\Carbon;
use App\Log;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Event::listen(\Illuminate\Auth\Events\Login::class, function ($event) {
            \Log::info("User login: {$event->user->name}");
            $c = new Log();
            $c->cveuser = $event->user->id;
            $c->login_at = Carbon::now();
            $c->save();
        });
        
        Event::listen(\Illuminate\Auth\Events\Logout::class, function ($event) {
            \Log::info("User logout: {$event->user->name}");
        });
    }
}
