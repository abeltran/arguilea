<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLogsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbllogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('cveuser');
            $table->foreign('cveuser')->references('id')->on('users');
            $table->datetime('login_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbllogs');
    }
}
