<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tblroles')->insert(
            array(
                ['role'=>'Administrador'],
                ['role'=>'Analista'],
            )
        );
    }
}
