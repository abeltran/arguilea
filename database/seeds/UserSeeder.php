<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name        	 = 'Benjamin Hernandez';
        $user->email         = 'benjamin@arguilea.com';
        $user->password      = 'secret';
        $user->cverole          = 1;
        $user->genre         = 2;
        $user->nickname      = 'bhdez';
        $user->save();

        $user = new User();
        $user->name        	 = 'Silvia Arce Rojas';
        $user->email         = 'silvia@arguilea.com';
        $user->password      = 'secret';
        $user->cverole          = 2;
        $user->genre         = 1;
        $user->nickname      = 'svarce';
        $user->save();
    }
}
