{!! Form::open(['route'=>[$pr->url(),$pr->id],'method'=> $pr->method(),'class'=>'app-form']) !!}
<div>
    {!! Form::label('nom','Nombre completo') !!}
    {!! Form::text('nom',$pr->name,['class'=>'form-control']) !!}
</div>
<div>
    {!! Form::label('email','Correo electrónico') !!}
    {!! Form::email('email',$pr->email,['class'=>'form-control']) !!}
</div>
@if(!$pr->id)
<div>
    {!! Form::label('password','Contraseña') !!}
    {!! Form::password('password', ['class'=>'form-control']) !!}
</div>
@endif
<div>
    {!! Form::label('nickname','Nickname') !!}
    {!! Form::text('nickname',$pr->nickname,['class'=>'form-control']) !!}
</div>
<div>
    {!! Form::label('cverole','Rol usuario') !!}
    {!! Form::select('cverole' ,['1' => 'Administrador', '2' => 'Analista'],  $pr->cverole, ['class'=>'form-control']); !!}
</div>
<div class="form-check form-check-inline my-4 px-1">
    {!! Form::label('genre','Femenino', ['class'=>'form-check-label px-1']) !!}
    {!!  Form::radio('genre', '1', $pr->genre == 1 ? true: false, ['class'=>'form-check-input'])!!}
</div>
<div class="form-check form-check-inline my-4 px-1">
    {!! Form::label('genre','Masculino', ['class'=>'form-check-label px-1']) !!}
    {!!  Form::radio('genre', '2', $pr->genre == 2 ? true: false, ['class'=>'form-check-input'])!!}
</div>
<br>
<div class="">
    <input type="submit" value="Guardar" class="btn btn-primary">
</div>
{!! Form::close() !!}