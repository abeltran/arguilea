@auth
{!! Form::open(['method'=>'DELETE', 'route'=> ['users.destroy',$pr->id], 'id' => 'usform'.$pr->id, 'onsubmit'=> 'return confirm("¿Estas seguro de eliminar este usuario?")']) !!}
 <a href="javascript:$('#usform{{ $pr->id }}').submit();" class="btn btn-danger" >
    <i class="fa fa-trash"></i>
 </a>
{!! Form::close() !!}
@endauth