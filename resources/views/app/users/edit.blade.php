@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card padding" style="padding: 1em;">
        <header>
            <h4>Editar usuario</h4>
            <p>{{ $pr->name }}</p>
        </header>
    <div class="card-body">
        @include('app.users.form')
    </div>
    </div>
</div>
@endsection