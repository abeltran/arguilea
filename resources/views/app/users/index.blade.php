@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{ route('users.create') }}" data-toggle="tooltip" data-placement="top" title="Agregar usuario" class="btn btn-success float-right">
                    <i class="fa fa-user-plus"></i>
                </a><br><br>
            <table id="tusers" class="table table-striped table-bordered table-condensed table-hover">
                <thead>
                    <th>Nombre</th>
                    <th>Correo</th>
                    <th>Acción</th>
                </thead>
                <tbody>
                    @forelse ($users as $u)
                    <tr>
                        <td>{{ $u->name }}</td>
                        <td>{{ $u->email }}</td>
                        <td class="text-center warning">
                            <a href="{{ url('users/'.$u->id.'/edit') }}" class="btn btn-warning">
                                <i class="fa fa-eye"></i>
                            </a>
                            @include('app.users.delete', ['pr'=>$u])
                        </td>
                    </tr>
                    @empty
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong>Aun no hay usuarios registrados</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                    @endforelse
                </tbody>
            </table>
            <div class="float-right">
            </div>
        </div>
        </div>
    </div>
    
@endsection
@section('scripts')
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
    $(document).ready( function () {
        $('#tusers').DataTable({
            "language": {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Spanish.json"
            }
        });
        $('[data-toggle="tooltip"]').tooltip();
    } );
</script>
@endsection