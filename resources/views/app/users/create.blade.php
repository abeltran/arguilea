@extends('layouts.app')
@section('content')
<div class="container">
    <div class="card padding" style="padding: 1em;">
        <header>
            <h4>Crear nuevo usuario</h4>
        </header>
    <div class="card-body">
        @include('app.users.form')
    </div>
    </div>
</div>
@endsection